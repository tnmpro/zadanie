
Zadanie https://gitlab.com/tnmpro/zadanie.git
Wymagania systemowe
NODE v18.16.0
NPM 9.5.1

Instalacja
1. Skopuj env.local to .env  w
2. Skonfiguruj MAGENTO_BACKEND_URL w pliku env
3. Odpal npm install --legacy-peer-deps
4. nmp run buildpack create-custom-origin ./
5. dodaj wygenerowaną domenę do hosts
6. npm run watch  

Niestety musiałem nadpisać CheckoutPage, SavedPaymentsPage ponieważ w venia, jest błąd, który uniemożliwia uruchomienie projektu.  
Istnieje możliwość napisania komponentów jako osobne extensions ładowane za pomocą package.json oraz użycie targetables,
tak aby po zainstalowaniu nie potrzebny był Override za pomocą wtyczki FoomanOverride
