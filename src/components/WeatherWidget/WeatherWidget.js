import React from 'react';
import { shape, string } from 'prop-types';

import { useStyle } from '@magento/venia-ui/lib/classify';
import defaultClasses from './weatherWidget.module.css';
import { useWeatherWidget } from './talons/useWeatherWidget';
import ResourceImage from '@magento/venia-ui/lib/components/Image/resourceImage';

const WeatherWidget = props => {
    const classes = useStyle(defaultClasses, props.classes);
    const { data } = useWeatherWidget();

    const toFahrenheit = celsius => (celsius * 1.8 + 32).toFixed(2);

    const image = data?.weather?.[0]?.icon ? (
        <ResourceImage
            resource={`https://openweathermap.org/img/wn/${
                data.weather[0].icon
            }.png`}
            alt="weather"
        />
    ) : null;

    const content =
        data && data.weather && data.main ? (
            <div className={classes.root}>
                <div className={classes.city}>
                    {data.name}, {data.sys.country}
                </div>
                <div className={classes.info}>
                    {image}
                    <span>{data.main.temp + '°C'}</span>
                    <span>{'(' + toFahrenheit(data.main.temp) + ')°F'}</span>
                </div>
            </div>
        ) : null;

    return content;
};

WeatherWidget.propTypes = {
    classes: shape({
        root: string,
        city: string,
        info: string
    })
};

export default WeatherWidget;
