import { useCallback, useEffect, useState } from 'react';

export const useWeatherWidget = () => {
    const apiKey = process.env.WEATHER_API_KEY;
    const defaultLocation = {
        latitude: 50.0833,
        longitude: 19.9167
    };
    const [location, setLocation] = useState(defaultLocation);
    const [data, setData] = useState(null);

    const fetchData = useCallback(
        (coords = defaultLocation) => {
            fetch(
                `https://api.openweathermap.org/data/2.5/weather?lat=${
                    coords.latitude
                }&lon=${coords.longitude}&appid=${apiKey}&units=metric`
            )
                .then(response => response.json())
                .then(setData)
                .catch(error => {
                    console.error('Error fetching weather data:', error);
                });
        },
        [apiKey]
    );

    const handleLocation = useCallback(() => {
        if (!apiKey) {
            console.error('API key is not set');
            return;
        }

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => {
                    const { latitude, longitude } = position.coords;
                    setLocation({ latitude, longitude });
                    fetchData(position.coords);
                },
                () => {
                    console.log(
                        'Unable to retrieve your location. Using default location.'
                    );
                    fetchData();
                }
            );
        } else {
            fetchData();
        }
    }, [apiKey, fetchData]);

    useEffect(() => {
        handleLocation();
    }, [handleLocation]);

    return { location, data };
};
