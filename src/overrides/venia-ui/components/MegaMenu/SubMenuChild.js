import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const SubMenuChild = ({ children, categoryUrlSuffix, classes, resourceUrl, keyboardProps, onNavigate }) => {

    const items = children.map((child, childIndex) => {
        const { url_path, isActive, name } = child;
        const categoryUrl = resourceUrl(
            `/${url_path}${categoryUrlSuffix || ''}`
        );
        return (
            <li key={childIndex}>
                <Link to={categoryUrl}
                      {...keyboardProps}
                      className={child.isActive ? classes.linkActive : classes.link}
                      onClick={onNavigate}>
                    {child.name}
                </Link>
            </li>
        );
    });


    return (
        <ul className={classes.submenuChild}>
            {items}
        </ul>
    );
};

SubMenuChild.propTypes = {
    children: PropTypes.array,
    categoryUrlSuffix: PropTypes.string,
    classes: PropTypes.object,
    resourceUrl: PropTypes.func,
    keyboardProps: PropTypes.object,
    onNavigate: PropTypes.func
};

export default SubMenuChild;