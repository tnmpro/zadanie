Wstępna analiza optymalizacji witryny 4f.pl
wykazała szereg obszarów wymagających poprawy, aby poprawić wydajność i dostosować stronę do standardów Core Web Vitals. Oto niektóre kluczowe kwestie i zalecenia:

* Desktop Szybkie łącze https://www.webpagetest.org/result/240415_BiDcMF_9ZZ/
* Desktop 3G https://www.webpagetest.org/result/240415_AiDcVS_CX8/
* Mobile 3G https://www.webpagetest.org/result/240415_AiDcN2_DK3/

![img.png](img.png)


* LCP (Largest Contentful Paint)
Długi czas ładowania największego elementu (12.063s) w przestrzeni above the fold (ATF). (https://4f.com.pl/media/webp_image/wysiwyg/SG/2404/PL_EN_SK_CZ_DE_UA_dsktp_multiactive.webp)
Wskazany obrazkiem, nie powinien znajdować się w przestrzeni ATF podczas ładowania strony.


* CLS (Cumulative Layout Shift)
Wysoki wynik CLS (1.716) wskazuje na przesunięcia układu strony podczas ładowania, co może być frustrujące dla użytkowników.
Content najprawdopodobniej jest ładowany z jednego bloku cms (widgetu).
Koniecznie jest rozdzielenie na strony na sekcje.
Tutaj dobrą praktyką jest, aby zastosować jak najmniej logiki na froncie dla elementu slidera tak, aby załadował się możliwie szybko.
Dobrze, aby pierwsze zdjęcie było ładowane możliwie bez użycia js, a slider był doładowany.
Widać także przesunięcia układu w miejscu, gdzie pojawiają się czcionki.
(Dla czcionek wskazany jest preload, aby przygotować przeglądarkę na kosztowną dla niej operacje)
Brak określonych rozmiarów przestrzeni, w której pojawić się mogą dynamicznie załadowane / wyrenderowane elementy (lazy load)

* INP (Interaction to Next Paint)
Duży wpływ na tę metrykę ma TBT Total Blocking Time (sporym winowajcą mogą być skrypty zewnętrzne)
Przy optymalizacji należy przeanalizować przy pomocy zakładki performence, zadania, które zajmują przeglądarce zbyt dużo czasu. Oraz wyeliminować skrypty blokujące.

![img_1.png](img_1.png)

Kluczowe jest także wykonywanie tylko tych operacji JS, które są potrzebne do wyświetlania elementów w przestrzeni ATF wszystkie inne znajdujące się BTF ładujemy na żądanie, lub lazy.

Przykładem skryptu blokującego ładowanie głównego wątku jest.
`<script src="https://cdn.jsdelivr.net/npm/seamless-scroll-polyfill@latest/lib/bundle.min.js"></script>`

![img_2.png](img_2.png)

* Dodatkowe uwagi

Web accessibility blokowanie skalowania jest źle odbierane przez Google. na stronie znajduje się
`<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">`

* Dodatkowe spostrzeżenia warte zastosowania,
stosowanie lazy load (np React.Lazy) dla elementów, które są below the fold,
Ładowanie skryptów na żądanie, (dopiero po wykonaniu interakcji przez użytkownika na stronie)
Na żądanie można ładować skrypty takie jak CookieBot które obecnie ładują się użytkownikowi, który nie wszedł jeszcze w interakcję z witryną. A samo ładowanie skryptu znacznie opóźnia oczekiwanie na całkowite załadowanie strony.

Niepokojące jest także wyczerpanie zasobu przeglądarki, przy normalnym użytkowaniu przeglądarka informuje nas o tym poprzez pokazanie błędu Out of Memory.
Problem ten wskazuje na potrzebę zoptymalizowania zarządzania pamięcią i zasobami.

![Out of Memory.png](Out%20of%20Memory.png)

To tylko część rzeczy, które zauważyłem podczas szybkiej analizy. Optymalizacja wymaga większej ilości testów, poprawek, a następnie kolejnych testów i poprawek :) jest to proces długotrwały, którego nie można podsumować w jednej analizie.
Przy optymalizacji należy pamiętać o tym, że należy uszczęśliwiać ludzi nie maszyny. (Często witryny z nie najlepszym wynikiem w lighthouse również mogą działać szybko, przykładem jest tu militaria.pl lub military.pl, nad którym miałem okazję pracować).
